﻿/*
 * Flash Mugshot module
 *
 * @author: Arnaud Bonnevigne <www.bonvga.net/contact>
 * @mail : bonvga@gmail.com
 *
 * External libraries or elements :
 * - base64 & encoding : http://www.bytearray.org/?p=27
 * - goo effect : http://incomplet.gskinner.com/index2.html#camgoo
 * - sound : http://www.sound-fishing.net/
 */

package {
    import flash.display.*;
    import flash.events.*;
    import flash.media.Camera;
    import flash.media.Video;
    import flash.media.Sound;
	import flash.net.*;
	import flash.utils.ByteArray;
	import flash.geom.*;
	import flash.external.*;
	import flash.filters.*;
	import flash.system.*;
	import org.smtp.encoding.Base64;
	import org.smtp.encoding.JPEGEncoder;
	
	public class Mugshot extends MovieClip {
		private const capture_width:Number = 640;
		private const capture_height:Number = 480;
		private const default_username:String = "Anonymous";

		private var video:Video;
		private var poster:URLLoader;
		private var camera:Camera;
		private var camera_name:String;
		private var sound:Boolean;
		private var preview:Boolean;
		private var jpeg_quality:Number;
		private var js_callback:String;
		private var post_url:String;
		private var old_position:Object;
		private var goo_point:Point;
		private var goo_rect:Rectangle;
		private var goo_bitmap:BitmapData;
		private var goo_bitmap_blur:BitmapData;
		private var goo_blur:BlurFilter;
		private var goo_displacement_map:DisplacementMapFilter;
		private var goo_enabled:Boolean;
		private var goo_started:Boolean;
		private var goo_level:Number;

		public function Mugshot() {
			goo_started = false;
			goo_level = 0;
			
			stage.scaleMode = StageScaleMode.SHOW_ALL;
            stage.align = StageAlign.TOP_LEFT;
			stage.quality = StageQuality.BEST;

			// hide brush movieclip
			mc_brush.visible = false;
			
			// hide info movieclip
			mc_info.visible = false;
			mc_info.mc_close.addEventListener(MouseEvent.CLICK, button_infoclose_click);
			
			// hide activity movieclip
			mc_activity.visible = false;
			
			// get flashvars
			var loader_username:String = root.loaderInfo.parameters.username;
			if (loader_username) {
				mc_gui.mc_text.mc_textfield.text = loader_username;
				mc_gui.mc_text.visible = false;
			} else {
				mc_gui.mc_text.mc_textfield.text = default_username;
				mc_gui.mc_text.visible = true;
			}
			var loader_sound:String = root.loaderInfo.parameters.sound;
			sound = true;
			if (loader_sound=="false") {
				sound = false;
			}
			js_callback = root.loaderInfo.parameters.js_callback;
			post_url = root.loaderInfo.parameters.post_url;
			if (!post_url) {
				display_message('\nWarning ! Mugshot using the default post url');
				post_url = 'http://localhost/mugshot_standalone/mugshot.php';
			}
			goo_enabled = true;
			if (root.loaderInfo.parameters.goo_enable == 'false') {
				goo_enabled = false;
			}
			preview = false;
			if (root.loaderInfo.parameters.preview == 'true') {
				preview = true;
			}
			jpeg_quality = 75;
			if (root.loaderInfo.parameters.jpeg_quality) {
				jpeg_quality = root.loaderInfo.parameters.jpeg_quality;
			}
					
			// register capture button event
			mc_gui.button_capture.addEventListener(MouseEvent.CLICK, button_capture_click);

			// register camera settings button event
			mc_gui.button_camera.addEventListener(MouseEvent.CLICK, button_camera_click);
			
			// register username textfield events
			mc_gui.mc_text.mc_textfield.addEventListener(FocusEvent.FOCUS_IN, textfield_username_focusin);
			mc_gui.mc_text.mc_textfield.addEventListener(FocusEvent.FOCUS_OUT, textfield_username_focusout);
			
			// init camera
			camera = Camera.getCamera();
            if (camera != null) {
				camera.setMode(capture_width, capture_height, 12);
				video = new Video(capture_width, capture_height);
                video.attachCamera(camera);
                mc_video.addChild(video);
				camera_name = camera.name;
            } else {
				display_message('\nNo webcam detected !');
            }
			
			// init poster
			poster = new URLLoader();
            poster.addEventListener(Event.COMPLETE, poster_complete);
            poster.addEventListener(SecurityErrorEvent.SECURITY_ERROR, poster_securityError);
            poster.addEventListener(HTTPStatusEvent.HTTP_STATUS, poster_httpStatus);
            poster.addEventListener(IOErrorEvent.IO_ERROR, poster_ioError);

			if (goo_enabled == true) {
				// init goo
				goo_rect = new Rectangle(0, 0, Math.floor(mc_video.width), Math.floor(mc_video.height));
				goo_bitmap = new BitmapData(goo_rect.width, goo_rect.height, false, 0x808080);
				goo_bitmap_blur = goo_bitmap.clone();
				goo_blur = new BlurFilter(8, 8, 2);
				goo_point = new Point(0, 0);
				goo_displacement_map = new DisplacementMapFilter(goo_bitmap_blur, goo_point, 2, 4, 100, 100, "clamp");
	
				// register goo events
				addEventListener(MouseEvent.MOUSE_DOWN, mouse_down);
				addEventListener(MouseEvent.MOUSE_UP, mouse_up);
				addEventListener(MouseEvent.MOUSE_MOVE, mouse_move);
				mc_gui.button_reset.addEventListener(MouseEvent.CLICK, goo_reset);
			} else {
				mc_gui.button_reset.visible = false;
			}
		}

		private function mouse_down(e:MouseEvent):void {
			old_position = {oldx:e.localX, oldy:e.localY};
			goo_started = true;
		}

		private function mouse_up(e:MouseEvent):void {
			goo_started = false;
		}

		private function mouse_move(e:MouseEvent):void {
			if (goo_started == false || goo_enabled == false) {
				return;
			}
			
			goo_level++;
			var dx:Number = e.localX - old_position.oldx;
			var dy:Number = e.localY - old_position.oldy;
			old_position = {oldx:e.localX, oldy:e.localY};
			
			// position the mouse and rotate according to direction of motion:
			mc_brush.rotation = (Math.atan2(dy, dx)) * 180 / Math.PI;
			mc_brush.x = e.localX;
			mc_brush.y = e.localY;
			
			// set up a color transform to color the brush according to direction of motion:
			var g:Number = 0x80 + Math.min(0x79, Math.max(-0x80, -dx*2));
			var b:Number = 0x80 + Math.min(0x79, Math.max(-0x80, -dy*2));
			var ct:ColorTransform = new ColorTransform(0, 0, 0, 1, 0x80, g, b, 0);
			
			// draw the brush onto the displacement map:
			goo_bitmap.draw(mc_brush, mc_brush.transform.matrix, ct, "hardlight");
			goo_apply();
		}
		
		private function goo_apply():void {
			if (goo_enabled == true) {
				goo_bitmap_blur.applyFilter(goo_bitmap, goo_rect, goo_point, goo_blur);
				mc_video.filters = [goo_displacement_map];
			}
		}
		
		private function goo_reset(e:MouseEvent):void {
			if (goo_enabled == true) {
				goo_bitmap.fillRect(goo_rect, 0x808080);
				goo_bitmap_blur.fillRect(goo_rect, 0x808080);
				goo_level = 0;
				goo_apply();
			}
		}
		
		private function textfield_username_focusin(e:FocusEvent):void {
			if (mc_gui.mc_text.mc_textfield.text == default_username) {
				mc_gui.mc_text.mc_textfield.text = '';
			}
		}
		
		private function textfield_username_focusout(e:FocusEvent):void {
			if (mc_gui.mc_text.mc_textfield.text == '') {
				mc_gui.mc_text.mc_textfield.text = default_username;
			}
		}
		
		private function poster_ioError(e:IOErrorEvent):void {
			mc_activity.stop();
			mc_activity.visible = false;
		}
		
		private function poster_securityError(e:SecurityErrorEvent):void {
			mc_activity.stop();
			mc_activity.visible = false;
			display_message('\nsecurityError');
		}
		
		private function poster_httpStatus(e:HTTPStatusEvent):void {
			if (e.status != 200 && e.status != 0) {
				mc_activity.stop();
				mc_activity.visible = false;
				display_message('\nhttpStatus '+e.status);
			}
		}
		
		private function poster_complete(e:Event):void {
			mc_activity.stop();
			mc_activity.visible = false;
			
			var loader:URLLoader = URLLoader(e.target);
			loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			//trace("completeHandler: " + loader.data);

			var vars:URLVariables = new URLVariables(loader.data);
			trace(vars.result);
			trace(vars.mid);
			trace(vars.murl);
			
			if (vars.result == "OK") {
				// appel d'une callback javascript
				if (js_callback) {
					ExternalInterface.call(js_callback, vars.mid, vars.murl, vars.mturl, ((preview)?'true':'false'));
				}
			} else {
				if (vars.result == "BLANK") {
					display_message('\nThe server have detected\na blank picture');
				} else {				
					display_message('\nError during storage');
				}
			}
		}
		
		public function display_message(val:String):void {
			mc_info.textfield.text = val;
			mc_info.visible = true;
		}
		
		public function button_infoclose_click(e:Event):void {
			mc_info.visible = false;
		}
		
		public function button_camera_click(e:Event):void {
			Security.showSettings(SecurityPanel.CAMERA);
		}
		
		public function button_capture_click(e:Event):void {
			mc_activity.visible = true;
			mc_activity.play();				

			if (sound == true) {
				var hsound_snap:sound_snap = new sound_snap();
				hsound_snap.play();
			}

			var bitmap:BitmapData = new BitmapData(capture_width,capture_height);
			bitmap.draw(mc_video);
			var byteArr:ByteArray = new ByteArray();
			var jpeg:JPEGEncoder = new JPEGEncoder(jpeg_quality);
			byteArr = jpeg.encode(bitmap);
			trace(byteArr.length);
			//byteArr.compress();
			//trace(byteArr.length);
			
			var request:URLRequest = new URLRequest(post_url);
			request.method = URLRequestMethod.POST;
            var variables:URLVariables = new URLVariables();
			variables.img = Base64.encode64(byteArr, false);
			trace('data length '+variables.img.length);
			variables.width = capture_width;
            variables.height = capture_height;
            variables.camera = camera_name;
            variables.username = mc_gui.mc_text.mc_textfield.text.substring(0,20);
			if (preview == true) {
				variables.preview = 'true';
			} else {
				variables.preview = 'false';
			}
			variables.goo_level = goo_level;
            variables.date = new Date().getTime();
            request.data = variables;
			try {
                poster.load(request);
            } catch (error:Error) {
				mc_activity.stop();
				mc_activity.visible = false;
				display_message('\nCan\'t contact the server !');
            }
		}
	}
}
<?php

function mugshot_views_data() {
  $data['mugshot']['table']['group']  = t('Mugshot');
  $data['mugshot']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['mugshot']['goo_level'] = array(
    'title' => t('Goo level'),
    'help' => t('Goo level.'),
    'field' => array(
      'handler' => 'mugshot_views_handler_mugshot_img',
      'click sortable' => TRUE,
     ),
     'filter' => array(
       'handler' => 'views_handler_filter_numeric',
       'label' => t('Value'),
       ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}

function mugshot_views_handler_mugshot_img($fieldinfo, $fielddata, $value, $data) {
  $node = node_load($data->nid);
  return image_display($node, $fielddata['options']);
}
?>

<?php

function mugshot_views_default_views() {
  $views = array();

$view = new view;
$view->name = 'mugshot';
$view->description = 'Mugshot';
$view->tag = 'mugshot';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = '0';
$view->api_version = 2;
$view->disabled = TRUE; // Edit this to true to make a default view disabled initially
$view->display = array();
  $display = new views_display;
  $display->id = 'default';
  $display->display_title = 'Defaults';
  $display->display_plugin = 'default';
  $display->position = '1';
  $display->display_options = array(
  'style_plugin' => 'grid',
  'style_options' => array(
    'columns' => '4',
    'alignment' => 'horizontal',
  ),
  'row_plugin' => 'node',
  'row_options' => array(
    'teaser' => 1,
    'links' => 1,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(
    'created' => array(
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'order' => 'DESC',
      'granularity' => 'second',
      'relationship' => 'none',
    ),
  ),
  'arguments' => array(),
  'filters' => array(
    'type' => array(
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'operator' => 'in',
      'value' => array(
        'mugshot' => 'mugshot',
      ),
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'relationship' => 'none',
      'expose_button' => array(
        'button' => 'Expose',
      ),
    ),
    'status' => array(
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'operator' => '=',
      'value' => 1,
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'relationship' => 'none',
      'expose_button' => array(
        'button' => 'Expose',
      ),
    ),
  ),
  'items_per_page' => 24,
  'title' => 'Mugshot gallery',
  'use_pager' => '1',
  'pager_element' => 0,
  'use_more' => 1,
  'offset' => 0,
  'empty' => 'No mugshot found',
  'empty_format' => '1',
  'link_display' => 'page',
);
$view->display['default'] = $display;
  $display = new views_display;
  $display->id = 'page';
  $display->display_title = '(page) Mugshot gallery';
  $display->display_plugin = 'page';
  $display->position = '2';
  $display->display_options = array(
  'defaults' => array(
    'access' => TRUE,
    'title' => TRUE,
    'header' => FALSE,
    'header_format' => FALSE,
    'header_empty' => FALSE,
    'footer' => TRUE,
    'footer_format' => TRUE,
    'footer_empty' => TRUE,
    'empty' => TRUE,
    'empty_format' => TRUE,
    'use_ajax' => TRUE,
    'items_per_page' => TRUE,
    'offset' => TRUE,
    'use_pager' => TRUE,
    'pager_element' => TRUE,
    'use_more' => TRUE,
    'distinct' => TRUE,
    'link_display' => TRUE,
    'style_plugin' => TRUE,
    'style_options' => TRUE,
    'row_plugin' => TRUE,
    'row_options' => TRUE,
    'relationships' => TRUE,
    'fields' => TRUE,
    'sorts' => TRUE,
    'arguments' => TRUE,
    'filters' => TRUE,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(),
  'arguments' => array(),
  'filters' => array(),
  'path' => 'mugshot/gallery/new',
  'menu' => array(
    'type' => 'normal',
    'title' => 'Mugshot gallery',
    'weight' => '0',
  ),
  'header' => '<div id="mugshot_add" class="mugshot_add">'.l(t('Add your own mugshot into the gallery !'),'node/add/mugshot').'</div>',
  'header_format' => '2',
  'header_empty' => 0,
);
$view->display['page'] = $display;
  $display = new views_display;
  $display->id = 'feed';
  $display->display_title = '(feed) Last mugshots feed';
  $display->display_plugin = 'feed';
  $display->position = '3';
  $display->display_options = array(
  'defaults' => array(
    'access' => TRUE,
    'title' => FALSE,
    'header' => TRUE,
    'header_format' => TRUE,
    'header_empty' => TRUE,
    'footer' => TRUE,
    'footer_format' => TRUE,
    'footer_empty' => TRUE,
    'empty' => TRUE,
    'empty_format' => TRUE,
    'use_ajax' => TRUE,
    'items_per_page' => FALSE,
    'offset' => FALSE,
    'use_pager' => FALSE,
    'pager_element' => FALSE,
    'use_more' => TRUE,
    'distinct' => TRUE,
    'link_display' => TRUE,
    'style_plugin' => FALSE,
    'style_options' => FALSE,
    'row_plugin' => FALSE,
    'row_options' => FALSE,
    'relationships' => TRUE,
    'fields' => TRUE,
    'sorts' => TRUE,
    'arguments' => TRUE,
    'filters' => TRUE,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(),
  'arguments' => array(),
  'filters' => array(),
  'displays' => array(),
  'style_plugin' => 'rss',
  'style_options' => array(
    'mission_description' => 0,
    'description' => 'Last mugshots feed',
  ),
  'row_plugin' => 'node_rss',
  'row_options' => array(
    'item_length' => 'teaser',
  ),
  'path' => 'mugshot/feed/rss.xml',
  'items_per_page' => 50,
  'offset' => 0,
  'use_pager' => '1',
  'pager_element' => 0,
  'title' => 'Last mugshots feed',
);
$view->display['feed'] = $display;
  $display = new views_display;
  $display->id = 'block';
  $display->display_title = '(block) Last mugshot';
  $display->display_plugin = 'block';
  $display->position = '4';
  $display->display_options = array(
  'defaults' => array(
    'access' => TRUE,
    'title' => FALSE,
    'header' => TRUE,
    'header_format' => TRUE,
    'header_empty' => TRUE,
    'footer' => TRUE,
    'footer_format' => TRUE,
    'footer_empty' => TRUE,
    'empty' => TRUE,
    'empty_format' => TRUE,
    'use_ajax' => TRUE,
    'items_per_page' => FALSE,
    'offset' => FALSE,
    'use_pager' => FALSE,
    'pager_element' => FALSE,
    'use_more' => TRUE,
    'distinct' => TRUE,
    'link_display' => TRUE,
    'style_plugin' => TRUE,
    'style_options' => TRUE,
    'row_plugin' => TRUE,
    'row_options' => TRUE,
    'relationships' => TRUE,
    'fields' => TRUE,
    'sorts' => TRUE,
    'arguments' => TRUE,
    'filters' => TRUE,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(),
  'arguments' => array(),
  'filters' => array(),
  'title' => 'Last mugshot',
  'items_per_page' => 1,
  'offset' => 0,
  'use_pager' => '0',
  'pager_element' => 0,
  'block_description' => 'Last mugshot',
);
$view->display['block'] = $display;
  $display = new views_display;
  $display->id = 'block_1';
  $display->display_title = '(block) Random mugshot';
  $display->display_plugin = 'block';
  $display->position = '5';
  $display->display_options = array(
  'defaults' => array(
    'access' => TRUE,
    'title' => FALSE,
    'header' => TRUE,
    'header_format' => TRUE,
    'header_empty' => TRUE,
    'footer' => TRUE,
    'footer_format' => TRUE,
    'footer_empty' => TRUE,
    'empty' => TRUE,
    'empty_format' => TRUE,
    'use_ajax' => TRUE,
    'items_per_page' => FALSE,
    'offset' => FALSE,
    'use_pager' => FALSE,
    'pager_element' => FALSE,
    'use_more' => TRUE,
    'distinct' => TRUE,
    'link_display' => TRUE,
    'style_plugin' => TRUE,
    'style_options' => TRUE,
    'row_plugin' => TRUE,
    'row_options' => TRUE,
    'relationships' => TRUE,
    'fields' => TRUE,
    'sorts' => FALSE,
    'arguments' => TRUE,
    'filters' => TRUE,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(
    'random' => array(
      'id' => 'random',
      'table' => 'views',
      'field' => 'random',
      'order' => 'ASC',
    ),
  ),
  'arguments' => array(),
  'filters' => array(),
  'title' => 'Random mugshot',
  'items_per_page' => 1,
  'offset' => 0,
  'use_pager' => '0',
  'pager_element' => 0,
  'block_description' => 'Random mugshot',
);
$view->display['block_1'] = $display;
  $display = new views_display;
  $display->id = 'page_1';
  $display->display_title = '(page) Mugshot best goo';
  $display->display_plugin = 'page';
  $display->position = '6';
  $display->display_options = array(
  'defaults' => array(
    'access' => TRUE,
    'title' => FALSE,
    'header' => FALSE,
    'header_format' => FALSE,
    'header_empty' => FALSE,
    'footer' => TRUE,
    'footer_format' => TRUE,
    'footer_empty' => TRUE,
    'empty' => TRUE,
    'empty_format' => TRUE,
    'use_ajax' => TRUE,
    'items_per_page' => TRUE,
    'offset' => TRUE,
    'use_pager' => TRUE,
    'pager_element' => TRUE,
    'use_more' => TRUE,
    'distinct' => TRUE,
    'link_display' => TRUE,
    'style_plugin' => TRUE,
    'style_options' => TRUE,
    'row_plugin' => TRUE,
    'row_options' => TRUE,
    'relationships' => TRUE,
    'fields' => TRUE,
    'sorts' => FALSE,
    'arguments' => TRUE,
    'filters' => TRUE,
  ),
  'relationships' => array(),
  'fields' => array(),
  'sorts' => array(
    'goo_level' => array(
      'id' => 'goo_level',
      'table' => 'mugshot',
      'field' => 'goo_level',
      'order' => 'DESC',
      'relationship' => 'none',
    ),
    'created' => array(
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'order' => 'DESC',
      'granularity' => 'second',
      'relationship' => 'none',
    ),
  ),
  'arguments' => array(),
  'filters' => array(),
  'path' => 'mugshot/gallery/goo',
  'title' => 'Mugshot best goo',
  'menu' => array(
    'type' => 'normal',
    'title' => 'Mugshot best goo',
    'weight' => '0',
  ),
  'header' => '<div id="mugshot_add" class="mugshot_add">'.l(t('Add your own mugshot into the gallery !'),'node/add/mugshot').'</div>',
  'header_format' => '2',
  'header_empty' => 0,
);
$view->display['page_1'] = $display;

//  'header' => '<div id="mugshot_add" class="mugshot_add">'.l(t('Add your own mugshot into the gallery !'),'node/add/mugshot').'</div>',

  $views[$view->name] = $view;
  return $views;
}
